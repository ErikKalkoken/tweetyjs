'use strict';

require('dotenv').config({ path: `${__dirname}/.env` });
const Discord = require('discord.js');
const { default: PQueue } = require('p-queue');

const config = require('./lib/config');
const { logger, logFilePath } = require('./lib/log');
const discord = new Discord.Client();
const { Sequelize } = require('sequelize');

// init database
const sequelize = new Sequelize('sqlite:data.db', { logging: false });
const { TwitterUser, Subscription } = require('./lib/models')(sequelize);
(async () => await sequelize.sync())();

// Queue for running job that checks for and posts new tweets
// We only want one instance of that job to run at any time
const queue = new PQueue({ concurrency: 1 });

/** Discord bot is logged in successfully and ready for action */
discord.on('ready', () => {
	logger.info('Logged in as %s.', discord.user.tag);
	if (config.updatesEnabled) {
		logger.info('Updates enabled.');
		queue.add(() => TwitterUser.postNewTweetsToDiscord(discord));
		setInterval(() => {
			// Do not queue more jobs if one is still running
			if (queue.size === 0) {
				queue.add(() => TwitterUser.postNewTweetsToDiscord(discord));
			}
		}, config.updateTimeout);
	} else {
		logger.info('Updates disabled.');
	}
});

/** Handle bot commands */
discord.on('message', async (message) => {
	if (!message.content.startsWith(config.prefix) || message.author.bot) return;

	const otherMember = await message.member.fetch();
	const myGuild = await discord.guilds.fetch(message.member.guild.id);
	if (
		myGuild.owner !== otherMember &&
		otherMember.roles.highest.position < myGuild.me.roles.highest.position
	) {
		await message.channel.send(
			'Sorry, but you are not authorized to use this bot.'
		);
		return;
	}

	const args = message.content.slice(config.prefix.length).trim().split(/ +/);
	const command = args.shift().toLowerCase();
	const param = args.shift();
	let msg;
	switch (command) {
		case 'add':
			if (param !== undefined) {
				const user = await TwitterUser.findOrCreateFromUsername(param);
				if (user == null) {
					msg = `**${param}** is not a known Twitter handle.`;
					break;
				}
				const [ _, created ] = await user.findOrCreateSubscription(
					message.channel.id
				);
				if (!created) {
					msg = `Already subscribed to **${user.name}**`;
				} else {
					msg = `Subscribed to **${user.name}**`;
				}
			} else {
				msg = 'Please provide a twitter user name like so: `!add Reuters`';
			}
			break;
		case 'fetch':
			queue.add(() => TwitterUser.postNewTweetsToDiscord(discord));
			msg = 'Updates scheduled.';
			break;
		case 'help':
			msg =
				'Current commands are:\n' +
				'`!add <handle>` - add Twitter feed to subscriptions\n' +
				'`!list` - show list of current subscriptions\n' +
				'`!remove <handle>` - remove Twitter feed from subscriptions';
			break;
		case 'list':
			const subscriptions = await Subscription.findForChannel(message.channel.id);
			if (subscriptions.length > 0) {
				await message.channel.send(
					`You currently have **${subscriptions.length}** ` +
						`subscription(s) in this channel:`
				);
				for (const subscription of subscriptions) {
					await subscription.postInfoToDiscord(discord);
				}
			} else {
				msg = 'There are currently no subscriptions in this channel.';
			}
			break;
		case 'remove':
			if (param !== undefined) {
				const isRemoved = await Subscription.removeSubscription(
					message.channel.id,
					param
				);
				if (!isRemoved) {
					msg = `No subscription found by the name **${param}**`;
				} else {
					msg = `Subscription **${param}** removed.`;
				}
			} else {
				msg = 'Please provide a twitter user name like so: `!remove @Reuters`';
			}
			break;
		case 'reset':
			const newDate = new Date(Date.now());
			newDate.setDate(1);
			newDate.setMonth(newDate.getMonth() - 1);
			for (const user of await TwitterUser.findAll()) {
				user.lastPostAt = newDate;
				await user.save();
			}
			msg = `lastPostAt reset to ${newDate.toDateString()} for all users.`;
			break;
		default:
			msg = 'Sorry, but I do not recognize that command.';
	}
	if (msg !== undefined) await message.channel.send(msg);
});

/** Make sure to log out when server is shut down */
function terminateServer() {
	logger.info('Logging out of Discord...');
	discord.destroy();
	logger.info('Shuting down database connection...');
	sequelize.close().then(() => logger.info('Server is shutdown.'));
	process.exit();
}
process.on('SIGTERM', terminateServer);
process.on('SIGINT', terminateServer);

console.log(
	`Started server tweetyjs\nLogging to: ${logFilePath}\nShutdown with CTRL-C`
);
logger.info('Starting server. Log level set to %s', config.logLevel);

// login bot and start event loop for Discord
discord.login(config.discordToken);
