'use strict';

const logger = require('winston');
const path = require('path');
const config = require('./config');

const myFormat = logger.format.printf(({ level, message, timestamp }) => {
	return `${timestamp} ${level.toUpperCase()} ${message}`;
});

const logFilePath = path.resolve(config.logFilePath, 'tweetyjs.log');

logger.configure({
	level: config.logLevel,
	format: logger.format.combine(
		logger.format.timestamp(),
		logger.format.splat(),
		myFormat
	),
	transports: [
		new logger.transports.File({
			filename: logFilePath,
			handleExceptions: true,
			maxsize: 10000000,
			maxFiles: 10
		})
	]
});

module.exports = { logger, logFilePath };
