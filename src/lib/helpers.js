'use strict';

// returns ISO 8601 string for given date without milliseconds
function dateToISOShort(date) {
	let myDate = new Date(date.getTime());
	myDate.setMilliseconds(0);
	return myDate;
}

module.exports = { dateToISOShort };
