'use strict';

const path = require('path');

const config = {
	prefix: process.env.PREFIX,
	discordToken: process.env.DISCORD_TOKEN,
	twitterToken: process.env.TWITTER_TOKEN,
	updateTimeout: Number(process.env.UPDATE_TIMEOUT) || 60000,
	updatesEnabled:
		process.env.UPDATES_ENABLED.toLocaleLowerCase() === 'true' ? true : false,
	showDebugInfos:
		process.env.SHOW_DEBUG_INFOS.toLocaleLowerCase() === 'true' ? true : false,
	maxTweetsPerUser: Number(process.env.MAX_TWEETS_PER_USER) || 50,
	logLevel: process.env.LOG_LEVEL || 'info',
	logFilePath: process.env.LOG_FILE_PATH || '.'
};

module.exports = config;
