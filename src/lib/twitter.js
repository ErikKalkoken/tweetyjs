'use strict';

const axios = require('axios');
const logger = require('winston');

const { dateToISOShort } = require('./helpers.js');

const TWITTER_API_BASE = 'https://api.twitter.com/2/';

/**
 * High level API for dealing with calls to the Twitter API
 */
class TwitterApi {
	constructor(bearerToken) {
		this.bearerToken = bearerToken;
	}
	/**
	 * Return the user object for a user from Twitter
	 * @param {string} username Name of Twitter user
	 * @return {Promise<object>} Infos about Twitter user
	 */
	async fetchUser(username) {
		const response = await this._twitterGet(`users/by/username/${username}`, {
			'user.fields': 'profile_image_url'
		});
		if (!response) return undefined;
		return response.data;
	}
	/**
	 * Return the latests tweets of a user on Twitter
	 * @param {number} userId ID of Twitter user to fetch tweets from
	 * @param {Date} startTime Earliest time of tweets to fetch
	 * @param {number} maxResults Maximum number of tweets to fetch. Must be between 5 and 100.
	 * @returns {Promise<Array>} List of tweets or undefined if no new tweets are found
	 */
	async fetchTimeline(userId, startTime = null, maxResults = null) {
		logger.debug('%s: Fetching timeline for user', userId);
		const params = {
			expansions: 'attachments.media_keys',
			'tweet.fields': 'created_at,entities',
			exclude: 'retweets,replies',
			'media.fields': 'preview_image_url,url'
		};
		if (startTime) params.start_time = dateToISOShort(startTime).toISOString();
		if (maxResults) {
			const maxResults2 = Number(maxResults);
			params.max_results =
				maxResults2 >= 5 && maxResults2 <= 100 ? maxResults2 : 50;
		}
		let response;
		response = await this._twitterGet(`users/${userId}/tweets`, params);
		if (!response || !('data' in response)) {
			logger.debug('%s: No new tweets', userId);
			return [];
		}
		const { data, includes } = response;
		logger.info('%s: Received %s new tweets', userId, data.length);
		logger.debug('%s: data: %s', userId, JSON.stringify(data, null, 2));
		logger.debug('%s: includes:', userId, JSON.stringify(includes, null, 2));
		if (includes === undefined) {
			return data;
		}
		let mediaLookup = {};
		if (includes !== undefined && 'media' in includes) {
			for (const media of includes.media) {
				mediaLookup[media.media_key] = media;
				if (media.type === 'photo') {
					mediaLookup[media.media_key].image_url = media.url;
				} else if (media.type === 'video') {
					mediaLookup[media.media_key].image_url = media.preview_image_url;
				}
			}
		}
		for (const tweet of data) {
			if ('attachments' in tweet && 'media_keys' in tweet.attachments) {
				let media = [];
				for (const media_key of tweet.attachments.media_keys) {
					if (media_key in mediaLookup) {
						media.push(mediaLookup[media_key]);
					}
				}
				tweet.media = media;
			}
		}
		// logger.debug(`${userId}: data':`, data);
		return data;
	}
	//** Make GET request to Twitter API and return result. */
	async _twitterGet(url, params) {
		const myURL = new URL(`${TWITTER_API_BASE}${url}`);
		myURL.search = new URLSearchParams(params).toString(params);
		const config = {
			headers: {
				Authorization: `Bearer ${this.bearerToken}`
			}
		};
		try {
			const { data } = await axios.get(myURL.href, config);
			return data;
		} catch (err) {
			if (err.response) {
				logger.error(err.response);
			} else if (err.request) {
				logger.error(err.request);
			} else {
				throw err;
			}
		}
	}
}

module.exports = TwitterApi;
