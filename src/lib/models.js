'use strict';

const { MessageEmbed } = require('discord.js');
const logger = require('winston');

const TwitterApi = require('./twitter');
const { Sequelize, Model, DataTypes } = require('sequelize');
const config = require('./config');

const sequelize = new Sequelize('sqlite:data.db', { logging: false });
const twitter = new TwitterApi(config.twitterToken);

const defineModels = (sequelize) => {
	class TwitterUser extends Model {
		/** Find or create new object from give username and return it. */
		static async findOrCreateFromUsername(username) {
			const userObj = await twitter.fetchUser(username);
			if (userObj == undefined) {
				return null;
			}
			const [ user, _ ] = await TwitterUser.findOrCreate({
				where: {
					id: userObj.id
				},
				defaults: {
					name: userObj.name,
					username: userObj.username,
					profileImageUrl: userObj.profile_image_url,
					lastPostAt: new Date(Date.now())
				}
			});
			return user;
		}
		/** Post new tweets for all subscriptions */
		static async postNewTweetsToDiscord(discord) {
			logger.info('Checking for new tweets from twitter users...');
			const users = await TwitterUser.findAll({
				include: { model: Subscription, required: true }
			});
			const allTweets = [];
			for (const user of users) {
				const tweets = await twitter.fetchTimeline(
					user.id,
					user.lastPostAt,
					config.maxTweetsPerUser
				);
				if (tweets !== undefined) {
					for (const tweet of tweets) {
						allTweets.push({
							created_at: tweet.created_at,
							tweet: tweet,
							user: user
						});
					}
				}
			}
			allTweets.sort((a, b) => {
				const a2 = new Date(a.created_at);
				const b2 = new Date(b.created_at);
				if (a2 < b2) return -1;
				if (a2 > b2) return 1;
				return 0;
			});
			const promises = [];
			for (const tweet of allTweets) {
				for (const subscription of tweet.user.Subscriptions) {
					promises.push(
						tweet.user._postTweetToChannel(
							discord,
							tweet.tweet,
							subscription.channelId
						)
					);
					await subscription.increment('tweetCount');
				}
				tweet.user.lastPostAt = new Date(
					new Date(tweet.created_at).getTime() + 1000
				); // adding 1 sec
				await tweet.user.increment('tweetCount');
			}
			await Promise.all(promises);
			for (const user of users) {
				await user.save();
			}
			logger.info('Updates completed.');
		}
		/** Post tweet to Discord channel */
		async _postTweetToChannel(discord, tweet, channelId) {
			let channel;
			try {
				channel = await discord.channels.fetch(channelId);
			} catch (e) {
				console.warn(e);
				return;
			}
			const tweetUrl = `https://twitter.com/i/web/status/${tweet.id}`;
			let footerText = `@${this.username}`;
			if (config.showDebugInfos) footerText += ` ${tweet.id}`;
			const embed = new MessageEmbed()
				.setAuthor(this.name, this.profileImageUrl, tweetUrl)
				.setDescription(`${tweet.text}`)
				.setFooter(footerText)
				.setTimestamp(tweet.created_at);
			if ('media' in tweet && tweet.media.length > 0) {
				const { image_url } = tweet.media[0];
				embed.setImage(image_url);
			}
			try {
				return await channel.send(embed);
			} catch (e) {
				console.error(e);
			}
		}
		/** Return URL to this user's home page on Twitter. */
		getHomeUrl() {
			return `https://twitter.com/${this.username}`;
		}
		/** Find or create subscription with given channel for this user and return it. */
		async findOrCreateSubscription(channelId) {
			return await Subscription.findOrCreate({
				where: {
					channelId: channelId,
					TwitterUserId: this.id
				}
			});
		}
	}

	TwitterUser.init(
		{
			id: {
				type: DataTypes.STRING,
				primaryKey: true
			},
			username: {
				type: DataTypes.STRING,
				allowNull: false
			},
			name: {
				type: DataTypes.STRING,
				allowNull: false
			},
			profileImageUrl: {
				type: DataTypes.STRING
			},
			lastPostAt: {
				type: DataTypes.DATE,
				allowNull: false,
				comment: 'Date of last posted tweet for this user'
			},
			tweetCount: {
				type: DataTypes.NUMBER,
				defaultValue: 0
			}
		},
		{
			sequelize,
			modelName: 'TwitterUser',
			indexes: [ { fields: [ 'username' ] } ]
		}
	);

	class Subscription extends Model {
		/** Return all subscriptions for given channel */
		static async findForChannel(channelId) {
			return await Subscription.findAll({
				where: { channelId: channelId },
				include: TwitterUser,
				order: [ [ TwitterUser, 'username', 'ASC' ] ]
			});
		}
		/** Remove subscription of given user for a channel */
		static async removeSubscription(channelId, username) {
			const subscription = await Subscription.findOne({
				where: {
					channelId: channelId
				},
				include: {
					model: TwitterUser,
					where: { username: username }
				}
			});
			if (subscription !== null) {
				await subscription.destroy();
				return true;
			} else {
				return false;
			}
		}
		/** Post info to subscribed Discord channel. */
		async postInfoToDiscord(discord) {
			let channel;
			try {
				channel = await discord.channels.fetch(this.channelId);
			} catch (e) {
				console.warn(e);
				return;
			}
			const user = this.TwitterUser;
			const lastTweetStr =
				user.tweetCount > 0 ? user.lastPostAt.toUTCString() : '(none yet)';
			const embed = new MessageEmbed()
				.setTitle(user.name)
				.setThumbnail(user.profileImageUrl)
				.setURL(user.getHomeUrl())
				.addFields(
					{ name: 'Handle', value: `@${user.username}`, inline: true },
					{ name: 'Tweets', value: this.tweetCount, inline: true },
					{ name: 'Subscribed', value: this.createdAt.toUTCString() },
					{ name: 'Last tweet', value: lastTweetStr }
				);
			try {
				await channel.send(embed);
			} catch (e) {
				console.error(e);
			}
		}
	}

	Subscription.init(
		{
			channelId: {
				type: DataTypes.STRING,
				allowNull: false
			},
			tweetCount: {
				type: DataTypes.NUMBER,
				defaultValue: 0
			}
		},
		{
			sequelize,
			modelName: 'Subscription',
			indexes: [ { fields: [ 'channelId' ] } ]
		}
	);

	TwitterUser.hasMany(Subscription, {
		foreignKey: {
			allowNull: false
		}
	});
	Subscription.belongsTo(TwitterUser);
	return { TwitterUser, Subscription };
};
module.exports = defineModels;
