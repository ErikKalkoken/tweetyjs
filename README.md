# tweetyjs

A Discord bot that automatically forwards Twitter tweets to a channel.

[![pipeline status](https://gitlab.com/ErikKalkoken/tweetyjs/badges/master/pipeline.svg)](https://gitlab.com/ErikKalkoken/tweetyjs/-/commits/master)
[![coverage report](https://gitlab.com/ErikKalkoken/tweetyjs/badges/master/coverage.svg)](https://gitlab.com/ErikKalkoken/tweetyjs/-/commits/master)

> **Note**<br>The current state of this project is **experimental**.

## Features

- Automatically forward tweets from subscribed Twitter handles to a Discord channel as they happen
- Subscribe or unsubscribe to Twitter handlers via bot command in a channel
- Show list of current subscription for a channel via bot command
- Show list of available bot commands with `!help`

## Installation (DRAFT)

Installation guide for Ubuntu.

### Prerequisites

- The following needs to be already installed on Ubnunt: Node, git, build essentials
- You have an account with a bearer token for the Twitter API v2
- You have created a Discord app with a bot user and have a bot token

### Installing the bot

- Create a dedicated user with `sudo adduser --disabled-login tweetyjs`
- switch to the new user with: `sudo su` and `su tweetyjs`
- cd into `/home/tweetyjs`
- clone the app's files into home with: `git clone https://gitlab.com/ErikKalkoken/tweetyjs`
- `cd tweetyjs`
- install the app with: `npm install`
- `cd src`
- copy sample config file: `cp .sample-env .env`
- add you discord bot token to DISCORD_TOKEN and your twitter bearer token to TWITTER_TOKEN in `.env`
- `cd ..`
- start the bot with `node src/server`

### Adding the bot to your discord server

- On the developer portal for your app under Oauth2 generate an OAUth2 URL. You need the scope `bot` and the following permissions:
  - View channels
  - Send Messages
  - Embed Links
  - Attach Files
  - Read Message History
  - Add Reaction

- Open the generated link in the browser

### Daemonize the app with PM2

- Install PM2: `sudo npm install pm2@latest -g`
- Switch to user tweetyjs
- Start the app with : `pm2 start src/server.js`
- Save the current process list: `pm2 save`
- Run : `pm2 startup systemd` to get the PM2 startup command for tweetyjs
- Switch to your sudo user
- Run the startup command received from `pm2 startup systemd` (last line, starts with sudo ...)
- Start the service with: `sudo systemctl start pm2-sammy`

### User Guide

To use the bot invite it into the channels you want to see Twitter feeds. Then use the bot commands to add or remove subscriptions. Use `!help` to get a list of available commands.

Authorization is done through role positions. Every user that has a role with a higher position then the bot and the server owner is allowed to use commands.
