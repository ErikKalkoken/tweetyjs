'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');
const { Sequelize } = require('sequelize');

const TwitterApi = require('../src/lib/twitter');

// set env
process.env.PREFIX = '!';
process.env.DISCORD_TOKEN = 'dummy1';
process.env.TWITTER_TOKEN = 'dummy2';
process.env.UPDATE_TIMEOUT = 30000;
process.env.UPDATES_ENABLED = false;
process.env.SHOW_DEBUG_INFOS = false;

// create new test database
const sequelize = new Sequelize('sqlite::memory:', { logging: false });
const { TwitterUser, Subscription } = require('../src/lib/models')(sequelize);
(async () => await sequelize.sync())();

class DiscordChannelStub {
	async send() {}
}

class DiscordChannelManagerStub {
	constructor(channelToReturn) {
		this.channelToReturn = channelToReturn;
	}
	async fetch() {
		return this.channelToReturn;
	}
}

class DiscordStub {
	constructor(channelToReturn) {
		this.channels = new DiscordChannelManagerStub(channelToReturn);
	}
}

describe('TwitterUser', function() {
	let twitterFetchUserStub;
	let twitterFetchTimelineStub;

	beforeEach(function() {
		twitterFetchUserStub = sinon.stub(TwitterApi.prototype, 'fetchUser');
		twitterFetchTimelineStub = sinon.stub(TwitterApi.prototype, 'fetchTimeline');
	});

	afterEach(function() {
		twitterFetchUserStub.restore();
		twitterFetchTimelineStub.restore();
	});

	describe('findOrCreateFromUsername()', function() {
		it('should create a new user', async function() {
			// given
			twitterFetchUserStub.returns({
				id: 123,
				username: 'my-username',
				name: 'my-fancy-name',
				profile_image_url: 'my-profile-image'
			});
			// when
			await TwitterUser.findOrCreateFromUsername('my-username');
			// then
			const user = await TwitterUser.findOne({ where: { id: 123 } });
			assert.strictEqual(user.username, 'my-username');
			assert.strictEqual(user.name, 'my-fancy-name');
			assert.strictEqual(user.profileImageUrl, 'my-profile-image');
		});
	});

	describe('postNewTweetsToDiscord()', function() {
		it('should post 4 tweets', async function() {
			// given
			const discordChannelStub = new DiscordChannelStub();
			const discordStub = new DiscordStub(discordChannelStub);
			const postStub = sinon.stub(TwitterUser.prototype, '_postTweetToChannel');
			await TwitterUser.create({
				id: 1001,
				name: 'dummy-name-1',
				username: 'dummy-username-1',
				lastPostAt: new Date(Date.now())
			});
			await TwitterUser.create({
				id: 1002,
				name: 'dummy-name-2',
				username: 'dummy-username-2',
				lastPostAt: new Date(Date.now()),
				tweetCount: 20
			});
			await Subscription.create({
				channelId: 42,
				TwitterUserId: 1001
			});
			await Subscription.create({
				channelId: 42,
				TwitterUserId: 1002,
				tweetCount: 10
			});
			twitterFetchTimelineStub
				.withArgs('1001', sinon.match.any)
				.resolves([
					{ id: 1, created_at: '2021-04-01T12:01:00.000Z' },
					{ id: 3, created_at: '2021-04-01T12:03:00.000Z' }
				])
				.withArgs('1002', sinon.match.any)
				.resolves([
					{ id: 2, created_at: '2021-04-01T12:02:00.000Z' },
					{ id: 4, created_at: '2021-04-01T12:04:00.000Z' }
				]);
			// when
			await TwitterUser.postNewTweetsToDiscord(discordStub);
			// then
			assert.strictEqual(postStub.callCount, 4);
			const tweetIds = postStub.args.map((obj) => obj[1].id);
			assert.deepEqual(tweetIds, [ 1, 2, 3, 4 ]);
			const user1 = await TwitterUser.findOne({ where: { id: 1001 } });
			assert.strictEqual(user1.tweetCount, 2);
			assert.deepEqual(user1.lastPostAt, new Date('2021-04-01T12:03:01.000Z'));
			const user2 = await TwitterUser.findOne({ where: { id: 1002 } });
			assert.strictEqual(user2.tweetCount, 22);
			assert.deepEqual(user2.lastPostAt, new Date('2021-04-01T12:04:01.000Z'));
			postStub.restore();
			const sub1 = await Subscription.findOne({
				where: {
					channelId: 42,
					TwitterUserId: 1001
				}
			});
			assert.strictEqual(sub1.tweetCount, 2);
			const sub2 = await Subscription.findOne({
				where: {
					channelId: 42,
					TwitterUserId: 1002
				}
			});
			assert.strictEqual(sub2.tweetCount, 12);
		});
	});
});

/*
describe('Subscription', function() {
	let twitterFetchUserStub;

	describe('postInfoToDiscord()', function() {
		it('should post info', async function() {
			// given
			const discordChannelStub = new DiscordChannelStub();
			const discordStub = new DiscordStub(discordChannelStub);
			await TwitterUser.create({
				id: 1001,
				name: 'dummy-name-1',
				username: 'dummy-username-1',
				lastPostAt: new Date(Date.now())
			});
			await Subscription.create({
				channelId: 42,
				TwitterUserId: 1001
			});
			subscription = await Subscription.findOne({
				where: { channelId: 42, TwitterUserId: 1001 },
				include: { model: TwitterUser }
			});
			// when
			subscription.postInfoToDiscord(discordStub);
			// then
			// assert.strictEqual(spy.callCount, 1);
		});
	});
});
*/
