const assert = require('assert');

const { dateToISOShort } = require('../src/lib/helpers.js');

describe('dateToISOShort', function() {
	it('should return date without milliseconds', function() {
		given = new Date(2021, 4, 23, 20, 15, 30, 99);
		expected = new Date(2021, 4, 23, 20, 15, 30, 0);
		assert.strictEqual(dateToISOShort(given).getTime(), expected.getTime());
	});
});
